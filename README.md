* REQUIREMENTS

    * Python 3
    
    * ROOT with the pyROOT library

First of all...
    You'll need ROOT for graphing.  If you have no idea what I'm talking about 
        https://root.cern.ch/  
    I'm not going to say it's the easiest install in the world. So ye be warned!  
    If you know what ROOT is, you'll need to make sure you have pyROOT installed
    properly:
        https://root.cern.ch/pyroot
    
    
* Pretty Simple Install
    That's all you really need.  It's just a script.  Plain and simple.  But
    that's what makes it so nice. 
    I recommend making a soft link in your path, something like:
    `ln -s graphIt.py $HOME/Applications/graphIt`
    or
    `ln -s graphIt.py $HOME/bin/graphIt`
    You can also do this with the units program too
    
* Input File:
    graphIt will accept any for the 4 data structures:
    * 1 Column: [y]
    * 2 Column: [x, y]  
    * 3 Column: [x, y, I]: column 3 (I) will be ignored  
    * 4 Column: [x, y, ex, ey]: ex and ey are the symetric errors of x and y (see -E option) 
    * The file may be comma seperated or tab seperated (see -t option)

* Arguments

        positional arguments:
            inputFile             Give a input file. A dual column, ASCII file of
                        numbers.
        optional arguments:
            -h, --help            show this help message and exit
            -o OUTPUTFILE, --outputFile OUTPUTFILE
                        Give the output file name. A ROOT file with TGraph
                        "graph" inside. root.cern.ch for info. (default =
                        "./graph.root")
            -t, --tabseperated    For tab seperated tables
            -E, --ignoreError     Ignore Errors even if given
                   
* graph.root File
    Once you run the script. It will generate a file graph.root (unless you 
    used the -o flag in which it's named something different)
    
        To open: # root graph.root
        To draw graph : [#] graph->Draw()
        To print data : [#] graph->Print()
        To quit:        [#] .q
    
    There's a bunch of other options you can learn.  
        https://root.cern.ch/doc/master/classTGraph.html
