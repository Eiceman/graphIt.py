#!/usr/local/bin/python3 
#################################################
# Author:	E. Gross			#
# Email:	eisen.gross@stonybrook.edu	#
# Date:		20 July 2017			#
# Verson:	3.0				#
# Purpose:	graphIt program for tsv		#
# Note:		Updated to Python3		#
#################################################
# Licensed under BSD terms
import argparse
import math, sys, csv

#Argument Parsing
argpars = argparse.ArgumentParser(
		prog = 'unit',
		description = 'Covert spectroscopic units')

argpars.add_argument('value', type=float, help="numerical value to convert")
argpars.add_argument('in_unit', choices=['nm', 'wn', 'THz'], help="Unit of input value")
argpars.add_argument('out_unit', choices=['nm', 'wn', 'THz', 'eV'], help="Unit of output")

args = argpars.parse_args()

#https://physics.nist.gov/cuu/Constants/Table/allascii.txt
h = 4.135667516E-15	#Planck's Constant eV*s
c = 2.99792458E8	#Speed of light
A = 6.022140857E23	#Avogadro Constant

def wn_to_nm(n):
	X = (n * 1E-7)**-1
	return X
def wn_to_hz(n):
	X = c * 100. * n
	X *= 1E-12
	return X
def wn_to_eV(n):
	X = 1. / (wn_to_nm(n)*1E-9)
	X = h*c * X
	return X

def nm_to_wn(n):
	X = (n)**-1 * 1E7
	return X
def hz_to_wn(n):
	X = n * (1./c) * (1./100)
	X *= 1E12
	return X

#def eV_to_wn(n):


if __name__ == "__main__":
	inwn = 0.0
	if args.in_unit == "nm":
		inwn = nm_to_wn(args.value)

	elif args.in_unit == "wn":
		inwn = args.value

	elif args.in_unit == "THz":
		inwn = hz_to_wn(args.value)

	else:
		print("ERROR: Unknown input unit")

	outValue = 0.0
	if args.out_unit == "nm":
		outValue = wn_to_nm(inwn)
		
	elif args.out_unit == "wn":
		outValue = inwn 

	elif args.out_unit == "THz":
		outValue = wn_to_hz(inwn)
	
	elif args.out_unit == "eV":
		outValue = wn_to_eV(inwn)

	else:
		print("ERROR: Unknown output unit")

	print("\t {.6f} {}".format(outValue, args.out_unit))

	






