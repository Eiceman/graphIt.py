#!/usr/local/bin/python3 
#################################################
# Author:	E. Gross			#
# Email:	eisen.gross@stonybrook.edu	#
# Date:		23 Feb 2018			#
# Verson:	3.5.1				#
# Purpose:	Makes a graph using 2 coln csv	#	
#		allows error for 4 coln		#
#		using ROOT library		#
#################################################
# Licensed under BSD terms
import argparse
import math, sys, csv
import datetime as dt

#Argument Parsing
argpars = argparse.ArgumentParser(
		prog = 'graphIt',
		description = 'Graphs data with ROOT in a csv file.  1 Column: [y] | 2 Columns: [x, y] | 4 Columns: [x, y, dx, dy]')

argpars.add_argument('inputFile', type=str, help='Give a input file. A dual column, ASCII file of numbers.')
argpars.add_argument('-o','--outputFile', type=str, help='Give the output file name. A ROOT file with TGraph "graph" inside.  root.cern.ch for info. (default = "./graph.root")')
argpars.add_argument('-t','--tabseperated', action='store_true', help="For tab seperated tables")
argpars.add_argument('-p','--histogram', action='store_true', help="[OFF] Make a histogram instead. For 1D data only, ignored otherwise.")
argpars.add_argument('-E','--ignoreError', action='store_true', help="Ignore Errors even if given")

args = argpars.parse_args()
args.histogram = False

from ROOT import TFile, TGraph, TGraphErrors, TH1F

#Copyright notice
print('\033[91m' + '\t \t graphIt.py (ver. 3.0)' + '\033[0m')
#print('\033[36m' + '\t Made by Eisen Gross' + '\033[0m')
#print('\033[31m' + '\t License: BSD clause 3, if you have to make money on this, show some love' + '\033[0m')
__version__ = "3.5.1"
__author__ = "E. Gross"
__email__ = "eisen.gross@stonybrook.edu"
__license__ = "BSD Clause 3"


histAllow = False
#The good stuff
dateString = str(dt.datetime.now().date())

with open(args.inputFile, 'rU') as ptsFile:

	#Open File, check data structure
	if args.tabseperated is True:
		pts = csv.reader(ptsFile, delimiter='\t')
		colNum = len(ptsFile.readline().split('\t'))
	else:
		pts = csv.reader(ptsFile)
		colNum = len(ptsFile.readline().split(','))

	ptsFile.seek(0)

	if colNum > 4:
		raise IndexError("I'm reading %d columns in that file. Must be <= 4" %colNum)
		sys.exit(1)
	elif colNum == 3:
		raise RuntimeWarning("There's 3 columns, ignoring row 3")

	if args.ignoreError is True and colNum > 2:
		colNum = 2

	elif colNum == 4:
		graph = TGraphErrors()
	elif 1 <= colNum <= 3:
		graph = TGraph()

	if args.histogram is True:
		if colNum == 1:
			histAllow = True
			histX = []

	x = 0
	y = 0
	ex = 0
	ey = 0

	i = 0
#Plot points
	for row in pts:
		graph.Set(i+1)

		if colNum == 1:
			x = i
			y = float(row[0])
			graph.SetPoint(i, x, y)
			if histAllow is True:
				histX.append(y)
			
		elif colNum == 2 or colNum == 3:
			x = float(row[0])
			y = float(row[1])
			graph.SetPoint(i, x, y)
		elif colNum == 4:
			x = float(row[0])
			y = float(row[1])
			ex = float(row[2])
			ey = float(row[3])
			graph.SetPoint(i, x, y)
			graph.SetPointError(i, ex, ey)
		else:
			raise RuntimeError("This is a developer bug, please put in ticket with reference 'l095'")

		i += 1
if histAllow is True:
	bins = int(math.ceil(max(histX)-min(histX)))
	hist1 = TH1F('hist1','Histogram',bins, min(histX), max(histX))
	for i in histX:
		hist1.Fill(i)

#Making file, and save
if args.outputFile == None:
	outputFile = dateString + '-graph.root'
else:
	outputFile = args.outputFile

outFile = TFile(outputFile, 'recreate')
graph.Write('g1')
if histAllow is True:
	hist1.Write('h1')
outFile.Close

